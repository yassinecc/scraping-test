const fs = require('fs');
const { findRegex, fetchPage, extractTitleAndLinks, cleanLinks } = require('./utils');
const args = process.argv.slice(2);
const input = fs.readFileSync(args[0]);

try {
  const twitterRegex = /\"(.*twitter\.com\/.*?\/status\/.*)\?/g;
  const titleRegex = /<title>(.*)<\/title>/g;

  const articleTitle = findRegex(input, titleRegex);
  const twitterLinks = findRegex(input, twitterRegex);

  const resultsObject = extractTitleAndLinks(input);

  fs.writeFileSync('./results.json', JSON.stringify(resultsObject, null, 2), 'utf-8');
} catch (e) {
  console.log('Error', e);
}

const linksRegex = /<a href=\"(.*actu17\.fr\/.*?\/)\">/g;

const articlesData = [];

Object.keys([...Array(407)]).map(pageNumber => {
  fetchPage(`https://www.actu17.fr/page/${pageNumber}`)
    .then(text => {
      const links = findRegex(text, linksRegex);
      const articlesLinks = cleanLinks(links);
      articlesLinks.map(articleLink => {
        setTimeout(() => {
          fetchPage(articleLink).then(articleText => {
            const resultsObject = extractTitleAndLinks(articleText);
            if (!resultsObject.title.includes('Actu17')) articlesData.push(resultsObject);
          }, 500);
        });
      });
    })
    .catch(console.log)
    .finally(() =>
      fs.writeFileSync(
        './results3.json',
        JSON.stringify([...new Set(articlesData)], null, 2),
        'utf-8'
      )
    );
});
