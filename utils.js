const fetch = require('node-fetch');

const findRegex = (string, regex) => {
  let matched = regex.exec(string);
  const result = [];
  while (matched && matched[1]) {
    result.push(matched[1]);
    matched = regex.exec(string);
  }
  return result.length === 1 ? result[0] : result;
};

const fetchPage = url => fetch(url).then(result => result.text());

const extractTitleAndLinks = input => {
  const twitterRegex = /\"(.*twitter\.com\/.*?\/status\/.*)\?/g;
  const titleRegex = /<title>(.*)<\/title>/g;

  const articleTitle = findRegex(input, titleRegex);
  const twitterLinks = findRegex(input, twitterRegex);

  return { title: articleTitle, links: twitterLinks };
};

const cleanLinks = linksArray => {
  return [
    ...new Set(
      linksArray.filter(
        link =>
          !link.includes('/author/') &&
          !link.includes('/conditions-generales/') &&
          !link.includes('/nous-contacter/') &&
          !link.includes('/politique-de-confidentialite/') &&
          !link.includes('/mentions-legales/')
      )
    ),
  ];
};

module.exports = { findRegex, fetchPage, extractTitleAndLinks, cleanLinks };
